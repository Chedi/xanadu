package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Events")
public class Event extends AbstractTable<Event> {
	private static final long serialVersionUID = 990881312719163662L;

	private long     id            ;
	private long     time          ;
	private Poseur   poster        ;
	private Campaign campaign      ;
	private String   image_hash    ;
	private Panel    event_panel   ;
	private Location event_location;

	public Event(final long id, final long time, final String image_hash, final Panel event_panel, final Location event_location, final Poseur poseur, final Campaign campaign) {
		super();
		this.id             = id            ;
		this.time           = time          ;
		this.poster         = poseur        ;
		this.campaign       = campaign      ;
		this.image_hash     = image_hash    ;
		this.event_panel    = event_panel   ;
		this.event_location = event_location;
	}

	public Event       () { super();     }
	public Event thiz  () { return this ;}

	@TablePK
	@TableGet(getName = "id"            )public long     getId           () { return id            ;}
	@TableGet(getName = "time"          )public long     getTime         () { return time          ;}
	@TableGet(getName = "poster"        )public Poseur   getPoster       () { return poster        ;}
	@TableGet(getName = "campaign"      )public Campaign getCompagne     () { return campaign      ;}
	@TableGet(getName = "image_hash"    )public String   getImageHash    () { return image_hash    ;}
	@TableGet(getName = "event_panel"   )public Panel    getEventPanel   () { return event_panel   ;}
	@TableGet(getName = "event_location")public Location getEventLocation() { return event_location;}

	@TableSet(getName = "id"            )public void setId           (final long     id            ) { this.id             = id            ;}
	@TableSet(getName = "time"          )public void setTime         (final long     time          ) { this.time           = time          ;}
	@TableSet(getName = "poster"        )public void setPoster       (final Poseur   poster        ) { this.poster         = poster        ;}
	@TableSet(getName = "campaign"      )public void setCompagne     (final Campaign campaign      ) { this.campaign       = campaign      ;}
	@TableSet(getName = "image_hash"    )public void setImageHash    (final String   image_hash    ) { this.image_hash     = image_hash    ;}
	@TableSet(getName = "event_panel"   )public void setEventPanel   (final Panel    event_panel   ) { this.event_panel    = event_panel   ;}
	@TableSet(getName = "event_location")public void setEventLocation(final Location event_location) { this.event_location = event_location;}
}