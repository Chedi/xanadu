package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "PanelTypes")
public class Type extends AbstractTable<Type> {
	private static final long serialVersionUID = -7941561952726758179L;

	private long   id   ;
	private String label;

	public Type(final long id, final String label){
		super();
		this.id    = id   ;
		this.label = label;
	}
	public Type            () { super()      ;}
	public Type   thiz     () { return this  ;}
	public String toString () { return label ;}

	@TablePK
	@TableGet( getName = "id" )public long     getId   () { return id   ;}
	@TableGet( getName = "label")public String getLabel() { return label;}

	@TableSet( getName = "id"   )public void setId   (final long   id   ) { this.id    = id   ;}
	@TableSet( getName = "label")public void setLabel(final String label) { this.label = label;}
}