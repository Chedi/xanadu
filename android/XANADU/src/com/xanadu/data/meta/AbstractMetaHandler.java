package com.xanadu.data.meta;

public abstract class AbstractMetaHandler<U> { 
	public abstract void getData(U data); 
}