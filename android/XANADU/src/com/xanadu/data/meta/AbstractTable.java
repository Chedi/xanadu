package com.xanadu.data.meta;

import java.util.Map                              ;
import java.util.List                             ;
import java.util.HashMap                          ;
import java.util.Iterator                         ;
import java.util.ArrayList                        ;
import java.util.Map.Entry                        ;
import java.io.Serializable                       ;
import java.lang.reflect.Method                   ;
import java.lang.reflect.InvocationTargetException;

import android.database.Cursor               ;
import android.content.ContentValues         ;
import android.database.sqlite.SQLiteDatabase;

public abstract class AbstractTable<T extends AbstractTable<T>> implements Serializable{
	public static final long   serialVersionUID = -6648880887117609116L;

	public static transient Map<String, List<Map<String, Method            >>> containersMtds = new HashMap<String, List<Map<String, Method            >>>();
	public static transient Map<String, List<Iterator<Entry<String, Method>>>> containersItrs = new HashMap<String, List<Iterator<Entry<String, Method>>>>();

	public abstract AbstractTable<T> thiz (          );
	public abstract long             getId(          );
	public abstract void             setId(long newId);

	public static<T extends AbstractTable<?>> String getTableName(final Class<T> clazz) throws AbstractTableException{
		String tableName;
		if(clazz.isAnnotationPresent(TableMeta.class)){ tableName = clazz.getAnnotation(TableMeta.class).tableName();}
		else                                          { tableName = "";                                 		     }
		return tableName;
	}

	public static<T extends AbstractTable<?>> String getFlushQuery(final Class<T> clazz) throws AbstractTableException{
		return "DELETE FROM " + getTableName(clazz);
	}

	public boolean equals(final Object obj) {
		boolean result = true;

		if      (this == obj)                     { result = true ; }
		else if (obj instanceof AbstractTable<?>) {
			try{
				final AbstractTable<?>                other            = (AbstractTable<?>) obj;
				final Iterator<Entry<String, Method>> tableColumnMapIT = getContainerMethodIter(getClass()).get(0);
				while (tableColumnMapIT.hasNext()){
					final Map.Entry<String, Method> getter = (Map.Entry<String, Method>)tableColumnMapIT.next();
						final Object src = getter.getValue().invoke(other);
						final Object dst = getter.getValue().invoke(this );
						result = src.equals(dst);
			}}catch(Exception e){
				result = false;
		}}else { result = false; }
		return result;
	}

	public int hashCode() {
		final int                             prime      = 31  ;
		int                                   result     = 1   ;
		Iterator<Entry<String, Method>> tableColumnMapIT = null;

		try                               { tableColumnMapIT = getContainerMethodIter(getClass()).get(0);}
		catch (AbstractTableException e1) {	}

		result = prime * result + (int) (getId() ^ (getId() >>> 32));
		if(tableColumnMapIT != null){
			while (tableColumnMapIT.hasNext()){
				final Map.Entry<String, Method> getter = (Map.Entry<String, Method>)tableColumnMapIT.next();
				Object value;
				try                 { value = getter.getValue().invoke(this );}
				catch (Exception e) { value = 0;                              }
				if(!value.getClass().isPrimitive()){
					result = prime * result + value.hashCode();
		}}}
		return result;
	}

	public static<T extends AbstractTable<?>> String buildTable(final Class<T> clazz) throws AbstractTableException {
		String                                fieldType        = null;
		final StringBuffer                    tableCreation    = new StringBuffer ("Create Table ");
		final Iterator<Entry<String, Method>> tableColumnMapIT = getContainerMethodIter(clazz).get(0);
		final AbstractTableException          failedParse      = new AbstractTableException("Failed to parse table field type");

		tableCreation.append(getTableName(clazz));
		tableCreation.append('(');

		while (tableColumnMapIT.hasNext()){
			final Map.Entry<String, Method> entry      = ((Map.Entry<String, Method>)tableColumnMapIT.next());
			final String                    key        = entry.getKey  ();
			final Method                    getter     = entry.getValue();
			final Class<?>                  ReturnType = getter.getReturnType();

			if(ReturnType.isPrimitive()){
				if     (ReturnType == float .class){ fieldType = "REAL"   ;}
				else if(ReturnType == long  .class){ fieldType = "INTEGER";}
				else if(ReturnType == int   .class){ fieldType = "INTEGER";}
			} else {
				if(ReturnType == String.class){ fieldType = "TEXT";}
				else{
					try {
						final Object instance = ReturnType.newInstance();
						if(instance instanceof AbstractTable<?>){
							fieldType = "INTEGER";
						}else{
							throw failedParse;
					}} catch (Exception e) {
						throw failedParse;
			}}}
			tableCreation.append(key      );
			tableCreation.append(' '      );
			tableCreation.append(fieldType);
			tableCreation.append(','      );
		}
		return tableCreation.substring(0, tableCreation.length()-1)+");";
	}

	private static<T extends AbstractTable<?>> List<Map<String, Method>> parse(final Class<T> clazz) throws AbstractTableException{
		final String tableName = getTableName(clazz);
		List<Map<String, Method>> result;
		if(containersMtds.containsKey(tableName)){
			result = containersMtds.get(tableName);
		}else{
			result = new ArrayList<Map<String, Method>>();
			final Map<String, Method> tableColumnGet  = new HashMap<String, Method>();
			final Map<String, Method> tableColumnSet  = new HashMap<String, Method>();
			final Method[]            tableMapmethods = clazz.getMethods();

			for (Method method : tableMapmethods){
				if (method.isAnnotationPresent(TableGet.class)){
					tableColumnGet.put(method.getAnnotation(TableGet.class).getName(), method);
				}else if (method.isAnnotationPresent(TableSet.class)){
					tableColumnSet.put(method.getAnnotation(TableSet.class).getName(), method);
				}}
			result.add(tableColumnGet);
			result.add(tableColumnSet);
			containersMtds.put(tableName, result);
		}
		return result;
	}

	public static<T extends AbstractTable<T>> boolean flushAll(final SQLiteDatabase database, final Class<T> clazz) throws InstantiationException, IllegalAccessException, AbstractTableException {
		boolean       result = true;
		final List<T> allObjects = getAllMappedOjects(database, clazz);

		for(T object : allObjects){
			result = object.delete(database);
			if(!result){
				break;
		}}
		return result;
	}

	private static<T extends AbstractTable<?>> List<Iterator<Entry<String, Method>>> getContainerMethodIter(final Class<T> clazz) throws AbstractTableException{
		final String              tableName   = getTableName(clazz);
		List<Map<String, Method>> parseResult = null;

		if(containersItrs.containsKey(tableName)){ parseResult = containersMtds.get(tableName);}
		else                                     { parseResult = parse(clazz);                 }
		final List<Iterator<Entry<String, Method>>> classMtdsItrs = new ArrayList<Iterator<Entry<String, Method>>>();
		classMtdsItrs.add(parseResult.get(0).entrySet().iterator());
		classMtdsItrs.add(parseResult.get(1).entrySet().iterator());
		containersItrs.put(tableName, classMtdsItrs);
		return classMtdsItrs;
	}

	public boolean insert(final SQLiteDatabase database) throws AbstractTableException{
		final ContentValues                   values     = new ContentValues();
		final Iterator<Entry<String, Method>> ColumnsMap = getContainerMethodIter(getClass()).get(0);
		boolean result = true;
		while (ColumnsMap.hasNext()){
			final Map.Entry<String, Method> entry = ((Map.Entry<String, Method>)ColumnsMap.next());
			final String key    = entry.getKey  ();
			final Method getter = entry.getValue();
			Object getterValue = null;

			try                 { getterValue = getter.invoke(this);}
			catch (Exception e) { result      = false;			    }

			if(getterValue instanceof AbstractTable<?>){
				final AbstractTable<?> castedGetterValue = (AbstractTable<?>) getterValue;
				if(castedGetterValue.insert(database)){ values.put(key, castedGetterValue.getId())               ;}
				else                                  { throw new AbstractTableException("Nested insertion failed");}
			}else if(getterValue != null){
				values.put(key, getterValue.toString());
		}}
		result = database.insert(getTableName(getClass()), null, values) != -1;
		return result;
	}

	public boolean delete(final SQLiteDatabase database) throws AbstractTableException{
		final Iterator<Entry<String, Method>> columnsMap = getContainerMethodIter(getClass()).get(0);

		long    id   = 0   ;
		boolean result = true;
		while (columnsMap.hasNext()){
			final Map.Entry<String, Method> entry = ((Map.Entry<String, Method>)columnsMap.next());
			final Method getter = entry.getValue();
			Object getterValue  = null;
			try                 { getterValue = getter.invoke(this);}
			catch (Exception e) { result      = false              ;}

			if     (getter.isAnnotationPresent(TablePK.class)){ id = (Long) getterValue;}
			else if(getterValue instanceof AbstractTable<?>  ){ ((AbstractTable<?>) getterValue).delete(database);}
		}
		if(database.delete(getTableName(getClass()), "id = ?", new String[]{String.valueOf(id)}) == 1){
			result = true;
		}
		return result;
	}

	public boolean upgrade(final SQLiteDatabase database) throws AbstractTableException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		boolean                               result           = true;
		final ContentValues                   values           = new ContentValues();
		final Iterator<Entry<String, Method>> tableColumnMapIT = getContainerMethodIter(getClass()).get(0);

		while (tableColumnMapIT.hasNext()){
			final Map.Entry<String, Method> entry = ((Map.Entry<String, Method>)tableColumnMapIT.next());
			final String key                      = entry.getKey  ();
			final Method getter                   = entry.getValue();
			final Object getterValue              = getter.invoke(this);
			if(getterValue instanceof AbstractTable<?>){
				final AbstractTable<?> castedGetterValue = (AbstractTable<?>) getterValue;
				result = castedGetterValue.insert(database);
				if(!result){ break ;}
				values.put(key, castedGetterValue.getId());
			}else{
				values.put(key, getterValue.toString());
		}}
		if(result){
			if(database.update(getTableName(this.getClass()), values, "id = ?", new String[]{String.valueOf(this.getId())}) == 1){
				result = true;
		}}else{ throw new AbstractTableException("Nested upgrade failed");}
		return result;
	}


	@SuppressWarnings({ "unchecked" })
	private static <U extends AbstractTable<U>> U reverseField(final SQLiteDatabase database, final Cursor queryResult, final Iterator<Entry<String, Method>> tableColumnMapIT, final Class<U> clazz) throws InstantiationException, IllegalAccessException, AbstractTableException{
		boolean status = false;
		final U result = (U) clazz.newInstance();
		try{
			while (tableColumnMapIT.hasNext()){
				final Map.Entry<String, Method> setter     = (Map.Entry<String, Method>)tableColumnMapIT.next();
				final Class<?>                  methodType = setter.getValue().getParameterTypes()[0];
				final int                       queryIndex = queryResult.getColumnIndex(setter.getKey());

				if     (methodType == String.class){ setter.getValue().invoke(result, queryResult.getString(queryIndex));}
				else if(methodType == float .class){ setter.getValue().invoke(result, queryResult.getFloat (queryIndex));}
				else if(methodType == long  .class){ setter.getValue().invoke(result, queryResult.getLong  (queryIndex));}
				else if(!methodType.isPrimitive()){
					final Object instance = methodType.newInstance();
					if(instance instanceof AbstractTable<?>){
						final AbstractTable<?> subComposition = getByPK(database, queryResult.getLong(queryIndex), ((AbstractTable<?>)instance).getClass());
						setter.getValue().invoke(result, subComposition);
					}else{
						status = false;
						break;
		}}}}catch(Exception e){ status = false;}
		if(status){ throw new AbstractTableException("field reversing failed");}
		return result;
	}

	public static <U extends AbstractTable<U>> U reverseContainer(final SQLiteDatabase database, final Cursor queryResult, final Class<U> clazz) throws AbstractTableException, InstantiationException, IllegalAccessException {
		U result = null ;

		if(queryResult.getCount() > 0){
			final Iterator<Entry<String, Method>> tableColumnMapIT = getContainerMethodIter(clazz).get(1);
			result = (U) AbstractTable.reverseField(database, queryResult, tableColumnMapIT, clazz);
		}
		return result;
	}

	public static<T extends AbstractTable<T>> List<T> getAllMappedOjects(final SQLiteDatabase database, final Class<T> clazz) throws InstantiationException, IllegalAccessException, AbstractTableException {
		final List<T> objects  = new ArrayList<T>();
		final Cursor cursor = database.rawQuery("SELECT * FROM "+ getTableName(clazz), null);
		cursor.moveToFirst();

		while(!cursor.isAfterLast()){
			final T result = reverseContainer(database, cursor, clazz);
			if(result != null){ objects.add(result);}
			cursor.moveToNext();
		}
		cursor.close();
		return objects;
	}

	public static<T extends AbstractTable<T>> T getByPK(final SQLiteDatabase database, final long primaryKey, final Class<T> clazz) throws AbstractTableException, InstantiationException, IllegalAccessException{
		T result;
		final String table       = clazz.getAnnotation(TableMeta.class).tableName();
		final String selectQuery = "SELECT * FROM "+table+" WHERE id = "+primaryKey;
		final Cursor queryResult = database.rawQuery(selectQuery, null);
		if(queryResult.getCount() > 0){
			queryResult.moveToFirst();
			result = reverseContainer(database, queryResult, clazz);
		}else{ throw new AbstractTableException("No element found");}
		queryResult.close();
		return result;
}}