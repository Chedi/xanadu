package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "UploadTasks")
public class UploadTask extends AbstractTable<UploadTask> {
	private static final long serialVersionUID = 567499121617502477L;

	private long  id       ;
	private Event event    ;
	private long  status   ;
	private long  endTime  ;
	private long  startTime;

	public UploadTask(final long id, final Event event, final long status, final long endTime, final long startTime) {
		super();
		this.id        = id       ;
		this.event     = event    ;
		this.status    = status   ;
		this.endTime   = endTime  ;
		this.startTime = startTime;
	}
	public UploadTask       () { super();     }
	public UploadTask thiz  () { return this ;}

	@TablePK
	@TableGet(getName = "id"       )public long  getId       () { return id        ;}
	@TableGet(getName = "event"    )public Event getEvent    () { return event     ;}
	@TableGet(getName = "status"   )public long  getStatus   () { return status    ;}
	@TableGet(getName = "endTime"  )public long  getEndTime  () { return endTime   ;}
	@TableGet(getName = "startTime")public long  getStartTime() { return startTime ;}

	@TableSet(getName = "id"       )public void setId       (final long  id       ) { this.id        = id        ;}
	@TableSet(getName = "event"    )public void setEvent    (final Event event    ) { this.event     = event     ;}
	@TableSet(getName = "status"   )public void setStatus   (final long  status   ) { this.status    = status    ;}
	@TableSet(getName = "endTime"  )public void setEndTime  (final long  endTime  ) { this.endTime   = endTime   ;}
	@TableSet(getName = "startTime")public void setStartTime(final long  startTime) { this.startTime = startTime ;}
}