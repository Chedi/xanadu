package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Locations")
public class Location extends AbstractTable<Location> {
	private static final long serialVersionUID = -7447157496513195666L;

	private long   id       ;
	private double latitude ;
	private double longitude;

	public Location(final long id, final double latitude, final double longitude) {
		super();
		this.id        = id       ;
		this.latitude  = latitude ;
		this.longitude = longitude;
	}

	public Location       () { super();     }
	public Location thiz  () { return this ;}

	@TablePK
	@TableGet(getName = "id"       )public long   getId       () { return id       ;}
	@TableGet(getName = "latitude" )public double getLatitude () { return latitude ;}
	@TableGet(getName = "longitude")public double getLongitude() { return longitude;}

	@TableSet(getName = "id"       )public void setId       (final long   id       ) { this.id        = id       ;}
	@TableSet(getName = "latitude" )public void setLatitude (final double latitude ) { this.latitude  = latitude ;}
	@TableSet(getName = "longitude")public void setLongitude(final double longitude) { this.longitude = longitude;}
}