package com.xanadu.application;

import android.app.Application                 ;
import android.content.Context                 ;
import android.content.SharedPreferences       ;
import android.content.SharedPreferences.Editor;

public class Xanadu extends Application {
	public static final int GPS_ACTIVITY         = 9999;
	public static final int SMS_ACTIVITY         = 9998;
	public static final int MAIN_ACTIVITY        = 9997;
	public static final int LOGIN_ACTIVITY       = 9996;
	public static final int EMAIL_ACTIVITY       = 9995;
	public static final int START_ACTIVITY       = 9994;
	public static final int CAMERA_ACTIVITY      = 9993;
	public static final int UPLOAD_ACTIVITY      = 9992;
	public static final int SYNCHRONIZE_ACTIVITY = 9991;

	public static final String FIRST_RUN            = "FIRST_RUN"          ;
	public static final String LOGIN_INFO           = "LOGIN_INFO"         ;
	public static final String GPS_STATUS           = "GPS_STATUS"         ;
	public static final String SYNC_STATUS          = "SYNC_STATUS"        ;
	public static final String HOST_SERVER          = "ns365885.ovh.net"   ;
	public static final String FTP_HOST_NAME        = "ns365885.ovh.net"   ;
	public static final String PICTURE_STATUS       = "PICTURE_STATUS"     ;
	public static final String OFFLINE_RELOAD       = "OFFLINE_RELOAD"     ;
	public static final String PASSWORD_RESET       = "PASSWORD_RESET"     ;
	public static final String APPLICATION_NAME     = "Xanadu"             ;
	public static final String UPLOAD_SERVICE_NAME  = "UPLOAD_SERVICE_NAME";
	public static final String LOGIN_USER_PASSWORD  = "LOGIN_USER_PASSWORD";
	public static final String GENERIC_PICTURE_NAME = "current.jpg"        ;

	public static final String HELP_DESK_PHONE  = "23164111"               ;
	public static final String HELP_DESK_EMAIL  = "chedi.toueiti@gmail.com";

	public static final int SMS_IDLE_STATUS          = 0;
	public static final int SMS_SENT_STATUS          = 1 >> 0;
	public static final int SMS_SENT_SUCCESSFUL      = 1 >> 1;
	public static final int SMS_SENT_UNSUCESSFUL     = 1 >> 2;
	public static final int SMS_DELIVERY_SUCESSFUL   = 1 >> 3;
	public static final int SMS_DELIVERY_UNSUCESSFUL = 1 >> 4;

	public static final String SMS_SENT         = "SMS_SENT"        ;
	public static final String SMS_STATUS       = "SMS_STATUS"      ;
	public static final String SMS_MESSAGE      = "SMS_MESSAGE"     ;
	public static final String SMS_DELIVERED    = "SMS_DELIVERED"   ;
	public static final String SMS_PHONE_SIM    = "SMS_PHONE_SIM"   ;
	public static final String SMS_DEVICE_ID    = "SMS_DEVICE_ID"   ;
	public static final String SMS_PHONE_NUMBER = "SMS_PHONE_NUMBER";
	public static final String SMS_EXTRA_STATUS = "SMS_EXTRA_STATUS";

	public static final String UPLOAD_EVENT        = "UPLOAD_EVENT"       ;
	public static final String UPLOAD_LOGIN        = "UPLOAD_LOGIN"       ;
	public static final String UPLOAD_PASSWORD     = "UPLOAD_PASSWORD"    ;
	public static final String UPLOAD_DEVICE_ID    = "UPLOAD_DEVICE_ID"   ;
	public static final String UPLOAD_LOCAL_FILE   = "UPLOAD_LOCAL_FILE"  ;
	public static final String UPLOAD_PANEL_TYPE   = "UPLOAD_PANEL_TYPE"  ;
	public static final String UPLOAD_REMOTE_FILE  = "UPLOAD_REMOTE_FILE" ;
	public static final String UPLOAD_PANEL_FORMAT = "UPLOAD_PANEL_FORMAT";
	public static final String UPLOAD_PHONE_NUMBER = "UPLOAD_PHONE_NUMBER";

	public static final int SMS_EXTRA_STATUS_SENT            = 0;
	public static final int SMS_EXTRA_STATUS_NULL_PDU        = 1;
	public static final int SMS_EXTRA_STATUS_RADIO_OFF       = 2;
	public static final int SMS_EXTRA_STATUS_NO_SERVICE      = 3;
	public static final int SMS_EXTRA_STATUS_GENERIC_FAILURE = 4;

	public static final String EMAIL_DOT       = "¤"              ;
	public static final String EMAIL_BODY      = "EMAIL_BODY"     ;
	public static final String EMAIL_SUBJECT   = "EMAIL_SUBJECT"  ;
	public static final String EMAIL_RECIPIENT = "EMAIL_RECIPIENT";

	public static final String CAPTURE_TIME      = "CAPTURE_TIME"     ;
	public static final String CURRENT_LOCATION  = "CURRENT_LOCATION" ;
	public static final String UPLOAD_SIM_SERIAL = "UPLOAD_SIM_SERIAL";

	public static final int HOST_PORT             = 80;

	public static final int UNINTIALIZED          = 0;
	public static final int FORCED_QUIT_STATE     = 1;
	public static final int SIM_MISSING_ERROR     = 2;
	public static final int MISSING_DATA_ERROR    = 3;
	public static final int NO_CONNECTION_ERROR   = 4;
	public static final int SYNCHRONIZATION_ERROR = 5;

	public static final String PASSWORD_RESET_RECIPIENTS = "chedi.toueiti@gmail.com¤m.guiga@gmail.com";

	public static final long UPLOAD_TASK_KO      = 0;
	public static final long UPLOAD_TASK_SENT    = 1;
	public static final long UPLOAD_TASK_PENDING = 2;


	Context           context    ;
	SharedPreferences preferences;

	public void onCreate() {
		super.onCreate();
		context     = this.getApplicationContext();
		preferences = context.getSharedPreferences(APPLICATION_NAME, MODE_PRIVATE);
	}

	public boolean getFirstRun() {
		return preferences.getBoolean(FIRST_RUN, true);
	}

	public void setRunned() {
		Editor edit = preferences.edit();
		edit.putBoolean(FIRST_RUN, false);
		edit.commit();
	}
}
