package com.xanadu.activities;

import android.util.Log                 ;
import android.os.Bundle                ;
import android.view.View                ;
import android.app.Activity             ;
import android.widget.Button            ;
import android.content.Intent           ;
import android.text.InputType           ;
import android.content.Context          ;
import android.widget.CheckBox          ;
import android.widget.EditText          ;
import android.view.View.OnClickListener;

import com.xanadu.data.Poseur            ;
import com.xanadu.application.R          ;
import com.xanadu.utils.SqlHelper        ;
import com.xanadu.application.Xanadu     ;
import com.xanadu.utils.SimpleAlertDialog;

public class LoginActivity extends Activity {
	private EditText loginInput    ;
	private EditText passwordInput ;
	private Button   validateLogin ;
	private CheckBox revealPassword;

	private String login   ;
	private String password;

	private Poseur poseur = null;

	private static Context context;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		context = this;

		loginInput     = (EditText) findViewById(R.id.login_input   );
		passwordInput  = (EditText) findViewById(R.id.password_input);
		validateLogin  = (Button  ) findViewById(R.id.validate_login);
		revealPassword = (CheckBox) findViewById(R.id.revealPassword);

		validateLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				final SqlHelper helper = new SqlHelper(context);
				try {
					login    = loginInput   .getText().toString();
					password = passwordInput.getText().toString();

					if(login.trim().length() > 0 && password.trim().length() > 0){
						poseur = helper.authentificateClient(login, password);
						if(poseur != null){ finish();}
						else              { new SimpleAlertDialog<Integer>(context, getString(R.string.authentication_failed));}
					} else {
						new SimpleAlertDialog<Integer>(context, getString(R.string.blank_login_data));
				}} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getMessage());
				} finally{
					helper.Close();
		}}});

		revealPassword.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				try {
					if (revealPassword.isChecked()) { passwordInput.setInputType(InputType.TYPE_CLASS_TEXT);}
					else                            { passwordInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);}
				} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getMessage());
		}}});
	}

	public void finish() {
		Intent data = new Intent();
		data.putExtra(Xanadu.LOGIN_INFO         , poseur  );
		data.putExtra(Xanadu.LOGIN_USER_PASSWORD, password);
		setResult(poseur != null ? RESULT_OK : RESULT_CANCELED, data);
		super.finish();
}}
