package com.xanadu.activities;

import com.xanadu.application.R     ;
import com.xanadu.application.Xanadu;

import android.util.Log                 ;
import android.os.Bundle                ;
import android.widget.Toast             ;
import android.app.Activity             ;
import android.content.Intent           ;
import android.content.Context          ;
import android.app.PendingIntent        ;
import android.app.ProgressDialog       ;
import android.content.IntentFilter     ;
import android.telephony.SmsManager     ;
import android.content.BroadcastReceiver;

public class SmsActivity extends Activity {
	private int     status      ;
	private Context context     ;
	private int     extra_status;

	private BroadcastReceiver sms_sent_receiver     ;
	private BroadcastReceiver sms_delivered_receiver;

	public void onCreate(Bundle saved_instance) {
		super.onCreate(saved_instance);

		context = this;

		status       = Xanadu.SMS_IDLE_STATUS;
		extra_status = Xanadu.SMS_IDLE_STATUS;

		final Bundle extras           = getIntent().getExtras();
		final String sms_message      = extras.getString(Xanadu.SMS_MESSAGE     );
		final String sms_phone_number = extras.getString(Xanadu.SMS_PHONE_NUMBER);

		final ProgressDialog progress_dialog = ProgressDialog.show(context, getString(R.string.please_wait), getString(R.string.sending_sms_request), true);

		sms_sent_receiver = new BroadcastReceiver(){
			public void onReceive(Context arg0, Intent arg1) {
				status |= Xanadu.SMS_SENT_STATUS;
				switch (getResultCode()) {
				    case Activity  .RESULT_OK                   : status |= Xanadu.SMS_SENT_SUCCESSFUL ; extra_status = Xanadu.SMS_EXTRA_STATUS_SENT           ; break;
					case SmsManager.RESULT_ERROR_NULL_PDU       : status |= Xanadu.SMS_SENT_UNSUCESSFUL; extra_status = Xanadu.SMS_EXTRA_STATUS_NULL_PDU       ; break;
					case SmsManager.RESULT_ERROR_RADIO_OFF      : status |= Xanadu.SMS_SENT_UNSUCESSFUL; extra_status = Xanadu.SMS_EXTRA_STATUS_RADIO_OFF      ; break;
					case SmsManager.RESULT_ERROR_NO_SERVICE     : status |= Xanadu.SMS_SENT_UNSUCESSFUL; extra_status = Xanadu.SMS_EXTRA_STATUS_NO_SERVICE     ; break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE: status |= Xanadu.SMS_SENT_UNSUCESSFUL; extra_status = Xanadu.SMS_EXTRA_STATUS_GENERIC_FAILURE; break;
			}
		}};

		sms_delivered_receiver = new BroadcastReceiver(){
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
					case Activity.RESULT_OK      : status |= Xanadu.SMS_DELIVERY_SUCESSFUL  ; break;
					case Activity.RESULT_CANCELED: status |= Xanadu.SMS_DELIVERY_UNSUCESSFUL; break;
			} finish();
		}};

		final PendingIntent sent_pending_intent      = PendingIntent.getBroadcast(this, 0, new Intent(Xanadu.SMS_SENT     ), 0);
		final PendingIntent delivered_pending_intent = PendingIntent.getBroadcast(this, 0, new Intent(Xanadu.SMS_DELIVERED), 0);

		registerReceiver(sms_sent_receiver     , new IntentFilter(Xanadu.SMS_SENT     ));
		registerReceiver(sms_delivered_receiver, new IntentFilter(Xanadu.SMS_DELIVERED));

		if
			(
				(sms_phone_number != null && sms_phone_number.length() > 0) &&
				(sms_message      != null && sms_message     .length() > 0)
			){

			new Thread() {
				public void run() {
					try {
						sleep(400);
						SmsManager sms_manager = SmsManager.getDefault();
						sms_manager.sendTextMessage(sms_phone_number, null, sms_message, sent_pending_intent, delivered_pending_intent);
					} catch (Exception e) {Log.d(getString(R.string.application_name), e.getMessage());}
					progress_dialog.dismiss();
			}}.start();
		} else {
			Toast.makeText(getBaseContext(), getString(R.string.invalid_sms_content), Toast.LENGTH_SHORT).show();
		}
	}

	public void finish() {
		final Intent data = new Intent();
		unregisterReceiver(sms_sent_receiver     );
		unregisterReceiver(sms_delivered_receiver);

		final boolean sms_ok = (status == (Xanadu.SMS_SENT_STATUS | Xanadu.SMS_SENT_SUCCESSFUL));
		setResult(sms_ok ? RESULT_OK : RESULT_CANCELED, data);

		data.putExtra(Xanadu.SMS_STATUS      , status      );
		data.putExtra(Xanadu.SMS_EXTRA_STATUS, extra_status);

		super.finish();
	}
}