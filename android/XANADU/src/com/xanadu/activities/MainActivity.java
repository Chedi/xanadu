package com.xanadu.activities;

import java.io.File       ;
import java.util.List     ;
import java.util.ArrayList;

import com.xanadu.data.Type                    ;
import com.xanadu.data.Panel                   ;
import com.xanadu.data.Event                   ;
import com.xanadu.data.Format                  ;
import com.xanadu.application.R                ;
import com.xanadu.data.Location                ;
import com.xanadu.data.Campaign                ;
import com.xanadu.utils.SqlHelper              ;
import com.xanadu.utils.SHAHashing             ;
import com.xanadu.application.Xanadu           ;
import com.xanadu.services.UploadService       ;
import com.xanadu.utils.ActivityLauncher       ;
import com.xanadu.utils.SimpleAlertDialog      ;
import com.xanadu.data.meta.AbstractMetaHandler;
import com.xanadu.utils.CustomSelectionListener;

import android.net.Uri                       ;
import android.util.Log                      ;
import android.os.Bundle                     ;
import android.view.View                     ;
import android.app.Activity                  ;
import android.widget.Button                 ;
import android.content.Intent                ;
import android.os.Environment                ;
import android.widget.Spinner                ;
import android.widget.EditText               ;
import android.content.Context               ;
import android.widget.ArrayAdapter           ;
import android.provider.MediaStore           ;
import android.view.View.OnClickListener     ;
import android.database.sqlite.SQLiteDatabase;

public class MainActivity extends Activity {
	private transient Context context     ;
	private transient boolean image_takeen;
	private transient boolean gps_acquired;

	private Panel    panel           ;
	private Event    event           ;
	private String   image_hash      ;
	private Long     capture_time    ;
	private Location capture_location;

	private transient Type   type             ;
	private transient Format format           ;
	private transient Button validate_panel   ;
	private transient String generic_file_path;

	private static final String path            = Environment.getExternalStorageDirectory().getPath();
	private static final File   pictures_folder = new File(path +"/"+ Xanadu.APPLICATION_NAME);

	private transient ActivityLauncher<GpsActivity, MainActivity> gps_activity;

	public void onCreate(final Bundle savedIS) {
		super.onCreate(savedIS);
		setContentView(R.layout.main);

		context = this;

		image_takeen                 = false;
		gps_acquired                 = false;
		validate_panel               = (Button  ) findViewById(R.id.validate_panel);

		final Spinner  compain       = (Spinner ) findViewById(R.id.compain       );
		final EditText panel_code    = (EditText) findViewById(R.id.panel_code    );
		final Spinner  panel_type    = (Spinner ) findViewById(R.id.panel_type    );
		final Spinner  panel_format  = (Spinner ) findViewById(R.id.panel_format  );
		final Button   image_capture = (Button  ) findViewById(R.id.image_capture );

		validate_panel.setEnabled(image_takeen);

		gps_activity = new ActivityLauncher<GpsActivity, MainActivity>(GpsActivity.class, Xanadu.GPS_ACTIVITY, this);

		final List        <Campaign> campaigns     = new ArrayList <Campaign>(SynchronizeActivity.getAllCompains    ());
		final List        <Type    > panel_types   = new ArrayList <Type    >(SynchronizeActivity.getAllPanelTypes  ());
		final List        <Format  > panel_formats = new ArrayList <Format  >(SynchronizeActivity.getAllPanelFormats());

		final ArrayAdapter<Campaign> compain_adapter       = new ArrayAdapter<Campaign>(this, android.R.layout.simple_spinner_dropdown_item, campaigns    );
		final ArrayAdapter<Type    > panel_types_adapter   = new ArrayAdapter<Type    >(this, android.R.layout.simple_spinner_dropdown_item, panel_types  );
		final ArrayAdapter<Format  > panel_formats_adapter = new ArrayAdapter<Format  >(this, android.R.layout.simple_spinner_dropdown_item, panel_formats);

		compain     .setAdapter(compain_adapter      );
		panel_type  .setAdapter(panel_types_adapter  );
		panel_format.setAdapter(panel_formats_adapter);

		panel_type.setOnItemSelectedListener(
				new CustomSelectionListener<Type>(
						new AbstractMetaHandler<Type>(){
							public void getData(final Type data) {
								type = (Type) data;
								validatePanelStatus();
		}}));

		panel_format.setOnItemSelectedListener(
				new CustomSelectionListener<Format>(
						new AbstractMetaHandler<Format>(){
							public void getData(final Format data) {
								format = (Format) data;
								validatePanelStatus();
		}}));

		image_capture.setOnClickListener(new OnClickListener() {
			public void onClick(final View view) {
				try {
					final boolean pictures_folder_exists = pictures_folder.isDirectory();
					if((!pictures_folder_exists && pictures_folder.mkdirs()) || pictures_folder_exists){
						final File   file          = new File(pictures_folder, Xanadu.GENERIC_PICTURE_NAME);
						generic_file_path          = file.getAbsolutePath();
						final Uri    file_uri      = Uri.fromFile(file);
						final Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
						startActivityForResult(camera_intent, Xanadu.CAMERA_ACTIVITY);
					}else{
						new SimpleAlertDialog<Integer>(context, getString(R.string.picture_shoot_failed));
					}
				} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getMessage());
		}}});

		validate_panel.setOnClickListener(new OnClickListener() {
			public void onClick(final View view) {
				final String panel_code_text = panel_code.getText().toString();
				if(panel_code_text.length() == 0){
					new SimpleAlertDialog<Integer>(context, getString(R.string.panel_code_required));
				} else {
					final SqlHelper      helper   = new SqlHelper(context);
					final SQLiteDatabase database = helper.getDatabase();
					try {
						panel = helper.findPanelByCode(panel_code_text);

						if(panel == null){
							new SimpleAlertDialog<Integer>(context, getString(R.string.invalid_panel_code));
						} else {
							final String picture_name = image_hash+".jpg";
							File old_picture = new File(generic_file_path);
							File new_picture = new File(pictures_folder+"/"+picture_name);

							if(!old_picture.renameTo(new_picture)){

							}

							final Campaign selected_campaign     = (Campaign) compain     .getSelectedItem();
							final Type     selected_panel_type   = (Type    ) panel_type  .getSelectedItem();
							final Format   selected_panel_format = (Format  ) panel_format.getSelectedItem();

							event = new Event(0, capture_time, image_hash, panel, capture_location, ProjectX.poseur, selected_campaign);
							if(event.insert(database)){
								final Intent upload_intent = new Intent(context, UploadService.class);

								final String password     = ProjectX.password;
								final String username     = ProjectX.poseur.getUser().getUsername();

								final String device_id  = ProjectX.siminfo.getDeviceId    ();
								final String sim_serial = ProjectX.siminfo.getSerialNumber();

								upload_intent.putExtra(Xanadu.UPLOAD_EVENT       , event                );
								upload_intent.putExtra(Xanadu.UPLOAD_LOGIN       , username             );
								upload_intent.putExtra(Xanadu.UPLOAD_PASSWORD    , password             );
								upload_intent.putExtra(Xanadu.UPLOAD_DEVICE_ID   , device_id            );
								upload_intent.putExtra(Xanadu.UPLOAD_SIM_SERIAL  , sim_serial           );
								upload_intent.putExtra(Xanadu.UPLOAD_LOCAL_FILE  , new_picture          );
								upload_intent.putExtra(Xanadu.UPLOAD_REMOTE_FILE , picture_name         );
								upload_intent.putExtra(Xanadu.UPLOAD_PANEL_TYPE  , selected_panel_type  );
								upload_intent.putExtra(Xanadu.UPLOAD_PANEL_FORMAT, selected_panel_format);

								startService(upload_intent);

								new SimpleAlertDialog<Integer>(context, getString(R.string.event_added_successfuly));
								validate_panel.setEnabled(false);
								panel_code.setText("");
							} else {
								new SimpleAlertDialog<Integer>(context, getString(R.string.event_insertion_failed));
					}}}catch (Exception e) {
						Log.d(getString(R.string.application_name), e.getMessage());
					} finally {
						helper.Close();
		}}}});
	}

	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		switch(requestCode){
			case Xanadu.CAMERA_ACTIVITY:
				image_takeen = (resultCode == RESULT_OK);
				if(image_takeen) { gps_activity.run()                                                               ;}
				else             { new SimpleAlertDialog<Integer>(context, getString(R.string.picture_shoot_failed));}
				break;
			case Xanadu.GPS_ACTIVITY:
				gps_acquired = (resultCode == RESULT_OK);
				if(gps_acquired){
					image_hash       = SHAHashing.SHAFileHashing(generic_file_path);
					capture_time     = (Long    ) data.getExtras().get(Xanadu.CAPTURE_TIME    );
					capture_location = (Location) data.getExtras().get(Xanadu.CURRENT_LOCATION);
				}
				validatePanelStatus();
				break;
			default:
				break;
	}}

	private void validatePanelStatus(){
		validate_panel.setEnabled(type != null && format != null && image_takeen && gps_acquired);
	}
}