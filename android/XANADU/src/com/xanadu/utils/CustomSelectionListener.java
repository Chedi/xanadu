package com.xanadu.utils;

import android.view.View                                ;
import android.widget.AdapterView                       ;
import android.widget.AdapterView.OnItemSelectedListener;

import com.xanadu.data.meta.AbstractMetaHandler;
import com.xanadu.data.meta.AbstractTable;

public class CustomSelectionListener<T extends AbstractTable<T>> implements OnItemSelectedListener{
	private final transient AbstractMetaHandler<T> handler;

	public CustomSelectionListener(final AbstractMetaHandler<T> handler) {
		super();
		this.handler = handler;
	}

	@SuppressWarnings("unchecked")
	public void onItemSelected(final AdapterView<?> parent, final View view, final int pos, final long identifier) {
		handler.getData((T) parent.getItemAtPosition(pos));
    }
	
    @SuppressWarnings("rawtypes")
	public void onNothingSelected(final AdapterView parent) { handler.getData(null); }
}