package com.xanadu.utils;

import java.util.List;

import com.xanadu.consumers.AbstractServiceConsumer;
import com.xanadu.data.Campaign;
import com.xanadu.data.Event;
import com.xanadu.data.Location;
import com.xanadu.data.Panel;
import com.xanadu.data.Format;
import com.xanadu.data.Type;
import com.xanadu.data.Poseur;
import com.xanadu.data.SimInfo;
import com.xanadu.data.UploadTask;
import com.xanadu.data.User;
import com.xanadu.data.meta.AbstractTable;
import com.xanadu.data.meta.AbstractTableException;

import android.util.Log                        ;
import android.database.Cursor                 ;
import android.content.Context                 ;
import android.database.sqlite.SQLiteDatabase  ;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {
	public static final String AUTHENTIFICATION_QUERY   = "SELECT p.id FROM Poseurs p INNER JOIN Users u ON p.user = u.id WHERE u.username = ?;";
	public static final String FIND_PANEL_BY_CODE_QUERY = "SELECT p.id FROM Panels p WHERE p.code = ?;";

	public static final int    DATABASE_VER   = 1;
	public static final String DATABASE_NAME  = "Xanadu";
	public static final String APPICATION_TAG = "Xanadu.ProjectX";

	private final transient Context        context ;
	private final transient SQLiteDatabase database;

	public SqlHelper(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VER);
		this.context  = context;
		this.database = getReadableDatabase();
	}

	public SqlHelper(final Context context, final String databaseName) {
		super(context, databaseName, null, 1);
		this.context  = context;
		this.database = getReadableDatabase();
	}

	public Panel findPanelByCode(String panel_code) {
		Panel panel = null;
		final Cursor result = database.rawQuery(FIND_PANEL_BY_CODE_QUERY, new String[]{panel_code});
		if(result.getCount() > 0){
			result.moveToNext();
			try {
				panel = getPanel(result.getLong(result.getColumnIndex("id")));
			} catch (Exception e) {
				e.printStackTrace();
				panel = null;
		}}
		result.close();
		return panel;
	}

	public void Close(){
		database.close();
	}

	public SQLiteDatabase getDatabase(){ return database; }

	public void onUpgrade(final SQLiteDatabase arg0, final int arg1, final int arg2) {
		try{
			context.deleteDatabase(DATABASE_NAME);
			database.execSQL(AbstractTable.buildTable(Type       .class));
			database.execSQL(AbstractTable.buildTable(User       .class));
			database.execSQL(AbstractTable.buildTable(Event      .class));
			database.execSQL(AbstractTable.buildTable(Panel      .class));
			database.execSQL(AbstractTable.buildTable(Format     .class));
			database.execSQL(AbstractTable.buildTable(Poseur     .class));
			database.execSQL(AbstractTable.buildTable(SimInfo    .class));
			database.execSQL(AbstractTable.buildTable(Campaign   .class));
			database.execSQL(AbstractTable.buildTable(Location   .class));
			database.execSQL(AbstractTable.buildTable(UploadTask .class));
		}catch(Exception e) { Log.d(APPICATION_TAG, e.getMessage());}
	}

	public void onCreate(final SQLiteDatabase database) {
		try{
			database.execSQL(AbstractTable.buildTable(Type       .class));
			database.execSQL(AbstractTable.buildTable(User       .class));
			database.execSQL(AbstractTable.buildTable(Event      .class));
			database.execSQL(AbstractTable.buildTable(Panel      .class));
			database.execSQL(AbstractTable.buildTable(Format     .class));
			database.execSQL(AbstractTable.buildTable(Poseur     .class));
			database.execSQL(AbstractTable.buildTable(SimInfo    .class));
			database.execSQL(AbstractTable.buildTable(Campaign   .class));
			database.execSQL(AbstractTable.buildTable(Location   .class));
			database.execSQL(AbstractTable.buildTable(UploadTask .class));
		} catch(Exception e) { Log.d(APPICATION_TAG, e.getMessage());
	}}

	public Poseur authentificateClient(final String login, final String plainPassword) throws AbstractTableException, InstantiationException, IllegalAccessException{
		Poseur poseur = null;
		final Cursor result = database.rawQuery(AUTHENTIFICATION_QUERY, new String[]{login});
		if(result.getCount() > 0){
			result.moveToNext();
			poseur = getPoseur(result.getLong(result.getColumnIndex("id")));

			final String   user_password  = poseur.getUser().getPassword();
			final String[] passwordTokens = user_password.split("\\$");

			boolean auth_status = true;
			try {
				final SHAHashing hasher = new SHAHashing(plainPassword, passwordTokens[1]);
				if(passwordTokens[2].equals(hasher.getHashedPassword())){ auth_status = true ;}
				else                                                    { auth_status = false;}
			} catch (Exception e) { auth_status = false; }
			if(!auth_status){
				poseur = null;
		}}
		result.close();
		return poseur;
	}

	public <T extends AbstractTable<T>> boolean synchronize(final AbstractServiceConsumer consumer) {
		List<T>      data     = null;
		boolean      result   = true;
		try {
			data = consumer.getAll();
			if(data != null && !data.isEmpty()){
				final T lamda = data.get(0);

				database.beginTransaction();
				database.execSQL(AbstractTable.getFlushQuery(lamda.getClass()));

				for (T item : data){
					item.insert(database);
				}
				database.setTransactionSuccessful();
				database.endTransaction();
				result = true;
			} else {
				result = false;
		}} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public User        getUser       (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return User       .getByPK(database, primaryKey, User       .class) ;}
	public Event       getEvent      (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Event      .getByPK(database, primaryKey, Event      .class) ;}
	public Panel       getPanel      (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Panel      .getByPK(database, primaryKey, Panel      .class) ;}
	public Poseur      getPoseur     (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Poseur     .getByPK(database, primaryKey, Poseur     .class) ;}
	public SimInfo     getSimInfo    (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return SimInfo    .getByPK(database, primaryKey, SimInfo    .class) ;}
	public Campaign    getCompagne   (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Campaign   .getByPK(database, primaryKey, Campaign   .class) ;}
	public Location    getLocation   (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Location   .getByPK(database, primaryKey, Location   .class) ;}
	public Type        getPanelType  (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Type       .getByPK(database, primaryKey, Type       .class) ;}
	public UploadTask  getUploadTask (final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return UploadTask .getByPK(database, primaryKey, UploadTask .class) ;}
	public Format      getPanelFormat(final long primaryKey) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Format     .getByPK(database, primaryKey, Format     .class) ;}

	public List<Event      > getAllEvents      (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Event      .getAllMappedOjects(database, Event      .class) ;}
	public List<Panel      > getAllPanels      (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Panel      .getAllMappedOjects(database, Panel      .class) ;}
	public List<Poseur     > getAllPoseurs     (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Poseur     .getAllMappedOjects(database, Poseur     .class) ;}
	public List<SimInfo    > getAllSimInfos    (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return SimInfo    .getAllMappedOjects(database, SimInfo    .class) ;}
	public List<Campaign   > getAllCompagnes   (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Campaign   .getAllMappedOjects(database, Campaign   .class) ;}
	public List<Location   > getAllLocations   (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Location   .getAllMappedOjects(database, Location   .class) ;}
	public List<Type       > getAllPanelTypes  (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Type       .getAllMappedOjects(database, Type       .class) ;}
	public List<UploadTask > getAllUploadTasks (       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return UploadTask .getAllMappedOjects(database, UploadTask .class) ;}
	public List<Format     > getAllPanelFormats(       ) throws InstantiationException, IllegalAccessException, AbstractTableException{ return Format     .getAllMappedOjects(database, Format     .class) ;}
}