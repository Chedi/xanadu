package com.xanadu.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NetworkConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(NetworkConnectivityReceiver.class.getSimpleName(), "action: "+ intent.getAction());
    }

}