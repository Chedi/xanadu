package com.xanadu.services;

import java.io.File           ;
import java.util.List         ;
import java.io.IOException    ;
import java.io.InputStream    ;
import java.util.ArrayList    ;
import java.io.Serializable   ;
import java.io.FileInputStream;

import org.apache.commons.net.ftp.FTP      ;
import org.apache.commons.net.ftp.FTPReply ;
import org.apache.commons.net.ftp.FTPClient;

import com.google.gson.Gson;

import com.xanadu.data.Type                     ;
import com.xanadu.data.Event                    ;
import com.xanadu.data.Format                   ;
import com.xanadu.data.UploadTask               ;
import com.xanadu.utils.SqlHelper               ;
import com.xanadu.application.Xanadu            ;
import com.xanadu.consumers.EventServiceConsumer;

import android.util.Log         ;
import android.content.Context  ;
import android.content.Intent   ;
import android.app.IntentService;

public class UploadService extends IntentService {
	private Context context;

	private Event  event       ;
	private String username    ;
	private String password    ;
	private String device_id   ;
	private Type   panel_type  ;
	private String sim_serial  ;
	private File   local_file  ;
	private String remote_file ;
	private Format panel_format;

	private static List<UploadTask> uploadPool = new ArrayList<UploadTask>();

	public UploadService() {
		super(Xanadu.UPLOAD_SERVICE_NAME);
	}

	protected void onHandleIntent(Intent intent) {
		event        = (Event ) intent.getExtras().get(Xanadu.UPLOAD_EVENT       );
		username     = (String) intent.getExtras().get(Xanadu.UPLOAD_LOGIN       );
		password     = (String) intent.getExtras().get(Xanadu.UPLOAD_PASSWORD    );
		device_id    = (String) intent.getExtras().get(Xanadu.UPLOAD_DEVICE_ID   );
		local_file   = (File  ) intent.getExtras().get(Xanadu.UPLOAD_LOCAL_FILE  );
		panel_type   = (Type  ) intent.getExtras().get(Xanadu.UPLOAD_PANEL_TYPE  );
		sim_serial   = (String) intent.getExtras().get(Xanadu.UPLOAD_SIM_SERIAL  );
		remote_file  = (String) intent.getExtras().get(Xanadu.UPLOAD_REMOTE_FILE );
		panel_format = (Format) intent.getExtras().get(Xanadu.UPLOAD_PANEL_FORMAT);

		uploadData();
	}

	void checkPool(){
		SqlHelper helper = new SqlHelper(context);
		try {
			List<UploadTask> database_pending_tasks = helper.getAllUploadTasks();
			for(UploadTask database_task : database_pending_tasks){
				if(database_task.getStatus() != Xanadu.UPLOAD_TASK_SENT){
				}
		}} catch (Exception e) {
			e.printStackTrace();
		}
		if(uploadPool.isEmpty()){
	}}

	private void uploadData(){
		FTPClient ftp_client = new FTPClient();
		try {
			ftp_client.connect(Xanadu.FTP_HOST_NAME);
			final int reply = ftp_client.getReplyCode();

			if(!FTPReply.isPositiveCompletion(reply)) {
				ftp_client.disconnect();
				Log.d(Xanadu.APPLICATION_NAME, "FTP server refused connection.");
			}

			if(!ftp_client.login(username, password)){
				ftp_client.logout();
			} else {
				ftp_client.setFileType(FTP.BINARY_FILE_TYPE);
				ftp_client.enterLocalPassiveMode();
				InputStream input;
				input = new FileInputStream(local_file);
				ftp_client.storeFile(remote_file, input);
				input.close();

				final Gson gson                        = new Gson();
				final EventPost eventPost              = new EventPost();
				final EventServiceConsumer ev_consumer = new EventServiceConsumer(Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);

				eventPost.setTime            (event.getTime                         ());
				eventPost.setPoster_id       (event.getPoster().getId               ());
				eventPost.setPosition_x      (event.getEventLocation().getLongitude ());
				eventPost.setPosition_y      (event.getEventLocation().getLatitude  ());
				eventPost.setImage_hash      (event.getImageHash                    ());
				eventPost.setCampaign_id     (event.getCompagne().getId             ());
				eventPost.setPanel_type_id   (panel_type.getId                      ());
				eventPost.setPanel_format_id (panel_format.getId                    ());

				final String request  = gson.toJson(eventPost);
				final String response = ev_consumer.setResult(ev_consumer.getAllApi(), request);
				Log.d(Xanadu.APPLICATION_NAME, response);
			}

		} catch(IOException e) {
			Log.d(Xanadu.APPLICATION_NAME, e.getStackTrace().toString());
		} finally {
			if(ftp_client.isConnected()) {
				try {
					ftp_client.disconnect();
				} catch(IOException ioe) {}
	}}}

	private class EventPost implements Serializable{
		private static final long serialVersionUID = -573348707684914886L;

		@SuppressWarnings("unused")private double time           ;
		@SuppressWarnings("unused")private double poster_id      ;
		@SuppressWarnings("unused")private double position_x     ;
		@SuppressWarnings("unused")private double position_y     ;
		@SuppressWarnings("unused")private double campaign_id    ;
		@SuppressWarnings("unused")private double panel_type_id  ;
		@SuppressWarnings("unused")private double panel_format_id;

		@SuppressWarnings("unused")private String image_hash;

		public void setTime            (double time           ) { this.time            = time           ;}
		public void setPoster_id       (double poster_id      ) { this.poster_id       = poster_id      ;}
		public void setPosition_x      (double position_x     ) { this.position_x      = position_x     ;}
		public void setPosition_y      (double position_y     ) { this.position_y      = position_y     ;}
		public void setCampaign_id     (double campaign_id    ) { this.campaign_id     = campaign_id    ;}
		public void setPanel_type_id   (double panel_type_id  ) { this.panel_type_id   = panel_type_id  ;}
		public void setPanel_format_id (double panel_format_id) { this.panel_format_id = panel_format_id;}

		public void setImage_hash      (String image_hash     ) { this.image_hash      = image_hash     ;}
	}
}