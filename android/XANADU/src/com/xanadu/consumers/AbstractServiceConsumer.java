package com.xanadu.consumers;

import java.util.List           ;
import java.lang.reflect.Type   ;
import java.io.BufferedReader   ;
import java.io.InputStreamReader;

import org.apache.http.HttpHost                        ;
import org.apache.http.HttpEntity                      ;
import org.apache.http.HttpResponse                    ;
import org.apache.http.auth.AuthScope                  ;
import org.apache.http.client.HttpClient               ;
import org.apache.http.impl.auth.BasicScheme           ;
import org.apache.http.client.methods.HttpGet          ;
import org.apache.http.entity.ByteArrayEntity          ;
import org.apache.http.client.methods.HttpPost         ;
import org.apache.http.protocol.BasicHttpContext       ;
import org.apache.http.impl.client.DefaultHttpClient   ;
import org.apache.http.impl.client.AbstractHttpClient  ;
import org.apache.http.auth.UsernamePasswordCredentials;

import android.util.Log                   ;
import com.google.gson.Gson               ;

import com.xanadu.application.Xanadu;
import com.xanadu.data.meta.AbstractTable;

public abstract class AbstractServiceConsumer {
	private final transient String           host        ;
	private final transient String           login       ;
	private final transient int              port        ;
	private final transient HttpClient       client      ;
	private final transient String           password    ;
	private final transient HttpHost         targetHost  ;
	private final transient BasicHttpContext localContext;

	public AbstractServiceConsumer(final String host, final int port, final String login, final String password){
		this.host     = host    ;
		this.port     = port    ;
		this.login    = login   ;
		this.password = password;

		client = new DefaultHttpClient();

		final AuthScope authScope = new AuthScope(host, port);
		final UsernamePasswordCredentials upc = new UsernamePasswordCredentials(login, password);
		((AbstractHttpClient) client).getCredentialsProvider().setCredentials(authScope, upc);

		localContext = new BasicHttpContext();

		final BasicScheme basicAuth = new BasicScheme();
		localContext.setAttribute("preemptive-auth", basicAuth);

		targetHost = new HttpHost(host, port, "http");
	}

	public String getResult(final String apiCall){
		final StringBuilder builder = new StringBuilder();
		try{
			final String getUrl = "http://"+getHost()+"/xanadu/api/"+apiCall;

			final HttpGet httpget = new HttpGet(getUrl);
			httpget.setHeader("Content-Type", "application/json");

			final HttpResponse   response = client.execute(targetHost, httpget, localContext);
			final HttpEntity     entity   = response.getEntity();
			final BufferedReader reader   = new BufferedReader(new InputStreamReader(entity.getContent()));
			while (true) {
				final String line = reader.readLine();
				if(line == null) { break               ;}
				else             { builder.append(line);}
			}
			reader.close();
		}catch(Exception e){ Log.d(Xanadu.APPLICATION_NAME, e.getMessage());}
		return builder.toString();
	}

	public String setResult(final String apiCall, final String request){
		final StringBuilder builder = new StringBuilder();
		try{
			final String setUrl = "http://"+getHost()+"/xanadu/api/"+apiCall;
			final HttpPost httppost = new HttpPost(setUrl);
			httppost.setHeader("Content-Type", "application/json");
			httppost.setEntity(new ByteArrayEntity(request.getBytes("UTF8")));

			final HttpResponse   response = client.execute(targetHost, httppost, localContext);
			final HttpEntity     entity   = response.getEntity();
			final BufferedReader reader   = new BufferedReader(new InputStreamReader(entity.getContent()));
			while (true) {
				final String line = reader.readLine();
				if(line == null) { break               ;}
				else             { builder.append(line);}
			}
			reader.close();
		}catch(Exception e){ Log.d(Xanadu.APPLICATION_NAME, e.getMessage());}
		return builder.toString();
	}

	@SuppressWarnings("unchecked")
	public <T extends AbstractTable<T>> List<T> getAll(){
		final Gson gson     = new Gson();
		List<T>    elements = null;
		try {
			String result = getResult(getAllApi()).toString();
			elements = (List<T>) gson.fromJson(result, getListTypeToken());
		}catch (Exception e) { Log.d(Xanadu.APPLICATION_NAME, "Error: " + e.getMessage());}
		return elements;
	}

	public <T extends AbstractTable<T>> void setAll(T data){
		final Gson gson     = new Gson();
		try {
			String result  = setResult(getAllApi(), gson.toJson(data)).toString();
			Log.d(Xanadu.APPLICATION_NAME, result);
		}catch (Exception e) { Log.d(Xanadu.APPLICATION_NAME, "Error: " + e.getMessage());}
	}

	public abstract String getAllApi        ();
	public abstract Type   getListTypeToken ();

	public int              getPort        () { return port        ;}
	public String           getHost        () { return host        ;}
	public String           getLogin       () { return login       ;}
	public HttpClient       getClient      () { return client      ;}
	public String           getPassword    () { return password    ;}
	public HttpHost         getTargetHost  () { return targetHost  ;}
	public BasicHttpContext getLocalContext() { return localContext;}

}