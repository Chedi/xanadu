package com.xanadu.consumers;

import java.util.List        ;
import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;

public class PanelTypeServiceConsumer extends AbstractServiceConsumer {
	public static final String ALL_API         = "panelTypes/"  ;
	public static final Type   LIST_TYPE_TOKEN = new TypeToken<List<com.xanadu.data.Type>>(){}.getType();

	public PanelTypeServiceConsumer(final String host, final int port, final String login, final String password) {
		super(host, port, login, password);
	}

	public String getAllApi       () { return ALL_API        ;}
	public Type   getListTypeToken() { return LIST_TYPE_TOKEN;}
}