package com.xanadu.consumers;

import java.util.List        ;
import java.lang.reflect.Type;

import com.xanadu.data.Event;
import com.google.gson.reflect.TypeToken;

public class EventServiceConsumer extends AbstractServiceConsumer {
		public static final String ALL_API         = "events/"   ;
		public static final Type   LIST_TYPE_TOKEN = new TypeToken<List<Event>>(){}.getType();

		public EventServiceConsumer(final String host, final int port, final String login, final String password) {
			super(host, port, login, password);
		}

		public String getAllApi       () { return ALL_API        ;}
		public Type   getListTypeToken() { return LIST_TYPE_TOKEN;}
	}