from piston.resource           import Resource
from piston.authentication     import HttpBasicAuthentication
from django.conf.urls.defaults import *

from xanadu.api.PanelHandler       import *
from xanadu.api.EventHandler       import *
from xanadu.api.PosterHandler      import *
from xanadu.api.CampaignHandler    import *
from xanadu.api.PanelTypeHandler   import *
from xanadu.api.PanelFormatHandler import *

auth = HttpBasicAuthentication(realm="Xanadu Releam")

events_handler        = Resource(EventHandler   )
open_campaigns_handler= Resource(CampaignTreePanelHandler)

panels_handler        = Resource(PanelHandler      , authentication=auth)
posters_handler       = Resource(PosterHandler     , authentication=auth)
campaigns_handler     = Resource(CampaignHandler   , authentication=auth)
panel_types_handler   = Resource(PanelTypeHandler  , authentication=auth)
panel_formats_handler = Resource(PanelFormatHandler, authentication=auth)

urlpatterns = patterns('',
	url(r'^panels/$'                               , panels_handler),
	url(r'^panels/(?P<panel_id>\d+)/$'             , panels_handler),

	url(r'^events/$'                               , events_handler),
	url(r'^events/(?P<event_id>\d+)$'              , events_handler),

	url(r'^posters/$'                              , posters_handler),
	url(r'^posters/(?P<poster_id>\d+)/$'           , posters_handler),

	url(r'^campaigns/$'                            , campaigns_handler),
	url(r'^campaigns/(?P<campaign_id>\d+)/$'       , campaigns_handler),
	url(r'^open/campaigns/$'                       , open_campaigns_handler),
	url(r'^open/campaigns/(?P<campaign_id>\d+)/$'  , open_campaigns_handler),

	url(r'^panelTypes/$'                           , panel_types_handler),
	url(r'^panelTypes/(?P<panel_type_id>\d+)/$'    , panel_types_handler),

	url(r'^panelFormats/$'                         , panel_formats_handler),
	url(r'^panelFormats/(?P<panel_format_id>\d+)/$', panel_formats_handler),
)