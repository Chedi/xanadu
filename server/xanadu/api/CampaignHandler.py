from datetime                 import datetime 
from piston.utils             import rc
from piston.handler           import BaseHandler
from xanadu.management.models import *

class CampaignHandler(BaseHandler):
	model           = Campaign
	allowed_methods = ('GET', 'POST')
	fields = ("id", "startDate", "endDate", "publie", "label", "Telecong", ("client",("raison_sociale", "user_id", "id",)))

	def read(self, request, campaign_id=None):
		if campaign_id != None:
			return self.model.objects.filter(id=campaign_id)
		else:
			return self.model.objects.exclude(endDate__lt=datetime.now())

class CampaignTreePanelHandler(BaseHandler):
	allowed_methods = ('GET',)

	def read(self, request):
		try:
			node_id = request.GET['node']
			if node_id and node_id != "0":
				return
		except Exception:
			pass

		result    = []
		campaigns = Campaign.objects.exclude(endDate__lt=datetime.now())
		for campaign in campaigns:
			campaign_event = []
			events = Event.objects.filter(campaign = campaign)
			for event in events:
				campaign_event.append({
					'id'         : event.id,
					'text'       : event.time,
					'campaign'   : event.campaign.label,
					'campaign_id': event.campaign.id,
					'poster'     : event.poster.user.get_full_name(),
					'image'      : event.image.get_display_url(),
					'position_x' : event.position.x,
					'position_y' : event.position.y,
					'leaf'       : True,
					})
			result.append({
				'id'       : campaign.id,
				'text'     : campaign.label,
				'children' : campaign_event,
				})
		return result