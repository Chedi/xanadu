from piston.utils             import rc
from piston.handler           import BaseHandler
from xanadu.management.models import *

class PosterHandler(BaseHandler):
	model           = Poster
	allowed_methods = ('GET', 'POST')
	fields = ("id",("user", ("username", "first_name", "last_name", "password","id")), "telephones")

	def read(self, request, poster_id=None):
		if poster_id != None:
			return self.model.objects.filter(id=poster_id)
		else:
			return self.model.objects.all()
