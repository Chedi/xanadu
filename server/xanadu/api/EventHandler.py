import os
import Image

from datetime                 import datetime
from piston.utils             import rc
from piston.handler           import BaseHandler
from django.core.files        import File
from django.contrib.gis.geos  import Point
from xanadu.management.models import *

class EventHandler(BaseHandler):
	model           = Event
	allowed_methods = ('GET', 'POST')

	def read(self, request, event_id = None):
		result = {
			"type": "FeatureCollection", 
			"features": [],
		}

		features = result['features']
		events = Event.objects.all()
		for event in events:
			features.append({
				"geometry" : {
					"type" : "Point",
					"coordinates" : event.coordinates()
				},
				"type": "Feature",
				"properties" : {
					"time"       : event.time,
					"image"      : event.image.get_thumbnail_url(),
					"poster_id"  : event.poster.id,
					"image_hash" : event.image_hash,
					"campaign_id": event.campaign.id,
				},
				"id" : event.id
				})
		return result

	def create(self, request):
		if request.content_type:
			data           = request.data

			time            = data['time'           ]
			poster_id       = data['poster_id'      ]
			position_x      = data['position_x'     ]
			position_y      = data['position_y'     ]
			image_hash      = data['image_hash'     ]
			campaign_id     = data['campaign_id'    ]
			panel_type_id   = data['panel_type_id'  ]
			panel_format_id = data['panel_format_id']

			operation_status_ok = True

			model_poster       = Poster     .objects.get(id=poster_id      )
			model_campaign     = Campaign   .objects.get(id=campaign_id    )
			model_panel_type   = PanelType  .objects.get(id=panel_type_id  )
			model_panel_format = PanelFormat.objects.get(id=panel_format_id)

			event_position_point = Point(y=position_y, x=position_x, srid=4326)
			event_position_point.transform(3857)

			if not model_poster or not model_campaign or not model_panel_type or not model_panel_format:
				return rc.BAD_REQUEST

			panel_code = "XXX"
			if Panel.objects.count() == 0:
				panel_code += "_0"
			else:
				panel_code += "_" + str(Panel.objects.latest('id').id + 1)

			panel_model = Panel(
				code     = panel_code,
				type     = model_panel_type,
				format   = model_panel_format,
				position = event_position_point,
			)

			panel_model.save()

			gallery = Gallery.objects.filter(title=model_campaign.label)

			if not gallery:
				gallery = Gallery.objects.create(
					tags        =model_campaign.label,
					title       =model_campaign.label,
					is_public   =True                ,
					title_slug  =model_campaign.label,
					description =model_campaign.label,)
			else:
				gallery = gallery[0]

			valid_image     = True
			image_file      = os.path.abspath(os.path.join(settings.UPLOAD_FTP_ROOT_PATH+model_poster.user.username ,image_hash+'.jpg'))
			not_found_image = File(open(os.path.join(settings.MEDIA_ROOT,'images/not_found.jpg')))

			try:
				image_obj=Image.open(image_file)
				image_obj.load
				image_obj.verify()
			except IOError:
				valid_image = False

			photo_title    = image_hash
			new_photo_file = os.path.abspath(os.path.join(settings.MEDIA_ROOT,"photologue/photos/"+photo_title+'.jpg'))

			try:
				already_extisting = Photo.objects.get(title=photo_title)
			except Photo.DoesNotExist:
				if valid_image:
					photo_image = File(open(image_file, 'r'))
				else:
					photo_image = not_found_image

				photo = Photo(
					tags       = model_campaign.label,
					title      = photo_title,
					caption    = photo_title,
					is_public  = True       ,
					title_slug = photo_title,)

				photo.image.save(photo_title+'.jpg', photo_image)
				gallery.photos.add(photo)

				event = Event(
					time       = datetime.fromtimestamp(time/1000),
					image      = photo,
					panel      = panel_model,
					poster     = model_poster,
					position   = event_position_point,
					campaign   = model_campaign,
					image_hash = image_hash,
				)

				event.save()
				return rc.CREATED
			return rc.BAD_REQUEST
		else:
			super(ExpressiveTestModel, self).create(request)
