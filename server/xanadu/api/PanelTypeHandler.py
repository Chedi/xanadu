from piston.handler           import BaseHandler
from xanadu.management.models import *

class PanelTypeHandler(BaseHandler):
	model           = PanelType
	allowed_methods = ('GET', 'POST')
	fields = ('id', 'label')

	def create(self, request):
		if request.content_type:
			data  = request.data
			image = self.model()
			panel = self.model(code = data['code'], type=data['type'], format=data['format'], position=data['position']).save()
			return rc.CREATED
		else:
			super(ExpressiveTestModel, self).create(request)

	def read(self, request, panel_type_id=None):
		base = PanelType.objects
		if panel_type_id:
			return base.filter(id=panel_type_id)
		else:
			return base.all()