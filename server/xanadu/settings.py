import os

DEBUG          = True
ADMIN_DIR      = os.path.dirname(__file__)
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = ('127.0.0.1',)
ADMINS = (('chedi.toueiti', 'chedi.toueiti@gmail.com'),)

ERROR_IMAGE           = 'not_fount.jpg'
AUTH_PROFILE_MODULE   = 'xanadu.management.Utilisateurs'
UPLOAD_FTP_ROOT_PATH  = "/var/ftp/xanadu/"

#CSRF_COOKIE_SECURE    = True
#SESSION_COOKIE_SECURE = True

MANAGERS = ADMINS

DATABASES = {
	'default': {
		'ENGINE'  : 'django.contrib.gis.db.backends.postgis',
		'NAME'    : 'xanadu',
		'USER'    : 'postgres',
		'PASSWORD': '',
		'HOST'    : 'localhost',
		'PORT'    : '5432',
	}
}

SITE_ID            = 1
USE_I18N           = True
USE_L10N           = True
MANAGERS           = ADMINS
TIME_ZONE          = 'Africa/Tunis'
MEDIA_URL          = 'http://ns365885.ovh.net/xanadu/media/'
MEDIA_ROOT         = '/var/www/django/xanadu/media/'
LANGUAGE_CODE      = 'en-US'
POSTGIS_VERSION    = (1, 5, 1)
STATICFILES_DIRS   = ()
ADMIN_MEDIA_ROOT   = '/var/www/django/xanadu/media/admin/'
ADMIN_MEDIA_PREFIX = 'http://ns365885.ovh.net/xanadu/media/admin/'
LOGIN_REDIRECT_URL = 'http://ns365885.ovh.net/xanadu/accounts/profile/'

SECRET_KEY = '_d6m@w7#7#nn7#w-yjyj-mnoc77qz1*t0)fdec40%d^n7cqa&_'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

TEMPLATE_CONTEXT_PROCESSORS += (
	'django.core.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.contrib.messages.context_processors.messages',
	'django.core.context_processors.request',
) 

TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'xanadu.urls'

TEMPLATE_DIRS = (
	os.path.join(ADMIN_DIR, 'templates'),
)

INSTALLED_APPS = (
	'admin_tools',
	'admin_tools.theming',
	'admin_tools.menu',
	'admin_tools.dashboard',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.gis',
	'django.contrib.admin',
	'xanadu.api',
	'photologue',
	'debug_toolbar',
	'xanadu.management',
)

DEBUG_TOOLBAR_CONFIG = {
	'INTERCEPT_REDIRECTS': False,
	'HIDE_DJANGO_SQL'    : False,
}
