from xanadu.management.models      import *
from xanadu.management.customAdmin import *
from django.contrib                import admin

admin.site.register( Event       , EventAdmin   )
admin.site.register( Panel       , PanelAdmin   )
admin.site.register( Poster                     )
admin.site.register( Client                     )
admin.site.register( Device                     )
admin.site.register( Campaign                   )
admin.site.register( PanelType                  )
admin.site.register( PanelFormat                )